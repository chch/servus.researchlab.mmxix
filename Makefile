this:
	./lib/mdsh/mk.sh servus.researchlab.mmxix.mdsh pdf
lokal:
	./utils/lokalize.sh -q src/tif/

font-hkgrotesk:
	./utils/fontinstall.sh -q --tex https://fontain.org/hkgrotesk/export/tex/hk-grotesk.tex.zip
	./utils/fontinstall.sh -q --ttf https://fontain.org/hkgrotesk/export/ttf/hk-grotesk.ttf.zip
font-inconsolata:
	./utils/fontinstall.sh -q --tex https://fontain.org/inconsolata/export/tex/inconsolata.tex.zip
	./utils/fontinstall.sh -q --ttf https://fontain.org/inconsolata/export/ttf/inconsolata.ttf.zip

pdf: lokal font-hkgrotesk font-inconsolata this

