
 OUT OF E-BUSINESS
 =================
 edited by Davide Bevilacqua, Katja Lux

 -------------------------------------------------------------------
 COPYRIGHT (C) 2020 servus.at + Authors
 -------------------------------------------------------------------
 If not stated otherwise permission is granted to copy, distribute
 and/or modify these documents under the  terms of the Creative 
 Commmons Attribution-ShareAlike 4.0 International License 

 -> http://creativecommons.org/licenses/by-sa/4.0/


 =============================================================== 

 EXECUTABLE CODE | COPYRIGHT (C) 2020 Christoph Haag
 ---------------
 lib/ utils/ 

 If not stated otherwise permission is granted to copy,
 distribute and/or modify these documents under the terms
 of the GNU General Public License as published by the
 Free Software Foundation; either version 3 of the License,
 or (at your option) any later version.
 --
 http://www.gnu.org/licenses/gpl.txt
 
 These documents are distributed in the hope that it
 will  be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE.
 
 Your fair use and other rights are not affected by the above.
 
 TRADEMARKS, QUOTED MATERIAL AND LINKED CONTENT
 IS COPYRIGHTED ACCORDING TO ITS RESPECTIVE OWNERS.
 
 E N J O Y !

 ===============================================================

